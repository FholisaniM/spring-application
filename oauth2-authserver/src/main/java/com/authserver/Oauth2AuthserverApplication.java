package com.authserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@Controller
@SessionAttributes("authorizationRequest")
@EnableResourceServer
public class Oauth2AuthserverApplication {

    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);
    @Autowired
    private TokenStore tokenStore;


    @Bean
    public  TokenStore tokenStore(){
        return new InMemoryTokenStore();
    }



  /* public JwtTokenStore jwtTokenStore(){
        JwtTokenStore tokenJwtStore =  new JwtTokenStore()
        return  new InMemoryTokenStore();
    }*/

    @RequestMapping(value = "/revoke-token", method = RequestMethod.GET)
    @ResponseBody
    public void logout(HttpServletRequest httpServletRequest){

        logger.info("Just loged out");
        String authHeader = httpServletRequest.getHeader("Authorization");
        if(authHeader !=null){
            logger.info("Just loged out delete");
            String tokenValue =authHeader.replace("Bearer","").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }
        logger.info("Just loged out: but null");
    }


    public static void main(String[] args) {
        SpringApplication.run(Oauth2AuthserverApplication.class, args);
    }




}
