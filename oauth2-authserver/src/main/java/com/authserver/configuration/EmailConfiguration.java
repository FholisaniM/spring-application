package com.authserver.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Configuration
@PropertySource("classpath:email.properties")
public class EmailConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


   @Bean
    public JavaMailSenderImpl emailSender(@Value("${smtp.host}") String emailHost,
                                          @Value("${smtp.port}") Integer emailPort,
                                          @Value("${smtp.username}") String username,
                                          @Value("${smtp.password}") String password) {


        JavaMailSenderImpl emailSender = new JavaMailSenderImpl();
        emailSender.setHost(emailHost);
        emailSender.setPort(emailPort);
        emailSender.setUsername(username);
        emailSender.setPassword(password);

        Properties javaMailProps = new Properties();
        javaMailProps.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailProps.put("mail.smtp.socketFactory.fallback", "false");
        javaMailProps.put("mail.smtp.starttls.enable", "true");
        javaMailProps.put("mail.smtp.host", "smtp.gmail.com");
        javaMailProps.put("mail.smtp.auth", "true");

        emailSender.setJavaMailProperties(javaMailProps);
        return emailSender;
    }


}
