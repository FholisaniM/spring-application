package com.authserver.controller.Application;

import com.authserver.Oauth2AuthserverApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created by Fholisani.Mashegana on 9/7/2016.
 */
@Controller
public class HomeController {
    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public RedirectView localRedirect() {
        logger.info("About to show home page <<<<<<<<<<<>>>>>>>>>>");
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("http://localhost:8778/#/");
        return redirectView;
    }
}
