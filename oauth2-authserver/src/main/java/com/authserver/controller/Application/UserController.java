package com.authserver.controller.Application;

import com.authserver.Oauth2AuthserverApplication;
import com.authserver.error.ValidationErrorBuilder;
import com.authserver.model.user.User;
import com.authserver.service.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fholisani.Mashegana on 9/7/2016.
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource messages;

    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Principal user(Principal user) {
        return user;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listAllUsers() {

        logger.info("About to return all users <<<<<<<<<<<>>>>>>>>>>");
        List<User> users = (List<User>) userService.findAllUsers();


        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        logger.info("Get Users: Return : " + users.toString());
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }


    @RequestMapping(value = "/user/", method = RequestMethod.POST,   consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody User user,
                                 BindingResult result,
                                 ModelMap model,
                                 UriComponentsBuilder ucBuilder) {
        String userId = "";

        logger.info("please check errors::::::::::::::::::::::::::::::::::::::::::::");
        if(result.hasErrors()){

            logger.info("Error : " +  ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result).toString()));
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result));
        }

        logger.info("please check errors :  password match");
        if(userService.isUserPasswordMatch(user.getPassword(), user.getConfirmPassword())){
            FieldError passwordError =new FieldError("user","password",
                    messages.getMessage("non.unique.password", new String[]{user.getEmail()}, Locale.getDefault()));
            result.addError(passwordError);
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result));

        }
        logger.info("please check errors :  uniqueness");

        if(!userService.isUserEmailUnique(user.getId(), user.getEmail())){
            FieldError emailError =new FieldError("user","email",
                    messages.getMessage("non.unique.email", new String[]{user.getEmail()}, Locale.getDefault()));
            result.addError(emailError);
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result));
        }


        logger.info("done: start creating user");
        try {
            logger.info("please create user");
            logger.info("please create user : " /*+ user.getName() + " : "+ user.getAddress() + " : "*/ + user.getEmail() );
            // User user = new User(email, name);
            userService.save(user);

            userId = String.valueOf(user.getId());
            logger.info("user created");
        }
        catch (Exception ex) {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);// ex.toString();
        }

        model.put("success", "User " + user.getFirstName() +/* + " "+ user.getLastName() + */" registered successfully");

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
        return new ResponseEntity<User>(headers, HttpStatus.CREATED);// + userId;
    }


    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/user/{id}/update", method = RequestMethod.PUT,   consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUser(@PathVariable("id") long id,
                                     @Valid @RequestBody User user,
                                     BindingResult result,
                                     ModelMap model,
                                     UriComponentsBuilder ucBuilder) {
        logger.info("Updating User " + id);


        User currentUser = userService.findById(id);

        if (currentUser==null) {
            logger.info("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        if(result.hasErrors()){

            logger.info("Error : " +  ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result).toString()));
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(result));
        }


        userService.updateUser(currentUser, user);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}/remove", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable("id") long id){
        logger.info("Delete User " + id);


        User user = userService.findById(id);

        if (user ==null) {
            logger.info("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.delete(user);
        return new ResponseEntity<User>(HttpStatus.OK);

    }
}
