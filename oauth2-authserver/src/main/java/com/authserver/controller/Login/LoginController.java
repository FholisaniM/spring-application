package com.authserver.controller.Login;

import com.authserver.Oauth2AuthserverApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Fholisani.Mashegana on 8/14/2016.
 */
@Controller
public class LoginController {
    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        logger.info("About to show Login <<<<<<<<<<<>>>>>>>>>>");
        model.addAttribute("name", "fholisani2");
        return "login";
    }
}