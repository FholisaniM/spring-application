package com.authserver.controller.Reset;

import com.authserver.MessageHelper.MessageHelper;
import com.authserver.Oauth2AuthserverApplication;
import com.authserver.model.reset.PasswordResetToken;
import com.authserver.model.user.User;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Fholisani.Mashegana on 8/14/2016.
 */
@Controller
@RequestMapping(value = ForgotpasswordController.FORGOT_PASSWORD)
public class ForgotpasswordController extends UserForgotPasswordController {


    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);
//{ "/", "/home" }
    //   public static  final  String USER_FORGOT_PASSWORD = "/forgotpassword";
    public static final String FORGOT_PASSWORD = "/forgotpassword";
    public static final String CHANGE_PASSWORD = "/changePassword";


    public static final String USER_FORGOT_PASSWORD = FORGOT_PASSWORD + "/{" +
            ForgotpasswordController.ID + "}" + "/{" + ForgotpasswordController.TOKEN + "}";


    @RequestMapping(method = RequestMethod.GET)
    public String forgotpassword(Model model) {

        User user = new User();
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        return addAttributesAndReturnForm(model, user, passwordResetToken);
    }

    private String addAttributesAndReturnForm(Model model, User user, PasswordResetToken passwordResetToken) {
        addAttributes(model, user, passwordResetToken);
        return FORGOT_PASSWORD;
    }

    private String addAttributesAndReturnChangeForm(Model model, User user, PasswordResetToken passwordResetToken) {
        addAttributes(model, user, passwordResetToken);
        return CHANGE_PASSWORD;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String resetPassword(@Validated({User.ValidRecovery.class}) @ModelAttribute(USER) User userEmail,
                                BindingResult result,
                                @ModelAttribute(USER_RESET) PasswordResetToken passwordResetToken,
                                BindingResult resultReset,
                                Model model,
                                HttpServletRequest request,
                                RedirectAttributes redirectAttributes) {

        logger.info("Just Posted Data");
        logger.info("User Model: lOOK for User : " + userEmail.getEmail());

        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, userEmail, passwordResetToken);
        }
        User user = userService.findByEmail(userEmail.getEmail());
        if (user == null) {
            logger.info("Two: lOOK for User Not found : " + userEmail.getEmail());
            // result.addError(new ObjectError(USER, "Email not registered! "));
            MessageHelper.addErrorAttribute(redirectAttributes, "auth.message.invalidUser");
            return "redirect:" + FORGOT_PASSWORD;
        }

        logger.info("Two: lOOK for User found : " + user.getFirstName());
        String tokenString = UUID.randomUUID().toString();


        String appUrl =
                "http://" + request.getServerName() +
                        ":" + request.getServerPort() +
                        request.getContextPath();
        SimpleMailMessage email =
                constructResetTokenEmail(appUrl, request.getLocale(), tokenString, user);

        try {
            logger.info("SENDING MAIL : " + userEmail.getEmail());
            mailSender.send(email);
            userService.createPasswordResetTokenForUser(user, tokenString);
            logger.info("Done SENDING MAIL : " + userEmail.getEmail());
        } catch (Exception e) {
            logger.info("SENDING MAIL: ERROR : " + userEmail.getEmail());

            MessageHelper.addErrorAttribute(redirectAttributes, "message.error");

            return "redirect:/" + FORGOT_PASSWORD;
        }

        logger.info("SENT LINK : " + userEmail.getEmail());
        MessageHelper.addSuccessAttribute(redirectAttributes, "message.resetPasswordEmail");
        logger.info("SENT LINK Message passed: " + userEmail.getEmail());
        return "redirect:/" + FORGOT_PASSWORD;

    }


    @RequestMapping(value = "/{" + ForgotpasswordController.ID + "}" + "/{" + ForgotpasswordController.TOKEN + "}",
            method = RequestMethod.GET)
    public String showChangePasswordPage(final Locale locale,
                                         Model model,
                                         @PathVariable(ForgotpasswordController.ID) final long id,
                                         @PathVariable(ForgotpasswordController.TOKEN) final String token,
                                         RedirectAttributes redirectAttributes) {

        logger.info("User token to authenticate : " + id + " : token " + token);
        PasswordResetToken passToken = new PasswordResetToken();
        User user = new User();

        logger.info("got User token to authenticate");
        try {
            passToken = userService.getPasswordResetToken(token);
            user = passToken.getUser();
            if ((passToken == null) || (user.getId() != id)) {
                logger.info(" User token is empty");
                final String message = messages.getMessage("auth.message.invalidToken", null, locale);
                MessageHelper.addErrorAttribute(redirectAttributes, "auth.message.invalidUser");
                return "redirect:/" + FORGOT_PASSWORD;
            }
            Calendar cal = Calendar.getInstance();
            if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
                model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
                logger.info("User token for authenticate failed 2: expired");
                MessageHelper.addErrorAttribute(redirectAttributes, "auth.message.expired");
                return "redirect:/" + FORGOT_PASSWORD;
            }


            logger.info(" User token is empty 2");

          //  Authentication auth = new UsernamePasswordAuthenticationToken(user, null, userDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
            //SecurityContextHolder.getContext().setAuthentication(auth);
            logger.info("User with token is authonticated");

        }catch (NullPointerException e){


            MessageHelper.addErrorAttribute(redirectAttributes, "auth.message.invalidToken");

            return "redirect:/" + FORGOT_PASSWORD;

        }catch (Exception e){
            MessageHelper.addErrorAttribute(redirectAttributes, "message.error");

            return "redirect:/" + FORGOT_PASSWORD;

        }


        model.addAttribute("token", passToken.getToken());

        return addAttributesAndReturnChangeForm(model, user, passToken);

    }


    @RequestMapping(value = "/{" + ForgotpasswordController.ID + "}" + "/{" + ForgotpasswordController.TOKEN + "}",
            method = RequestMethod.POST)
    public String savePasswordPage(final Locale locale,
                                         Model model,
                                         @PathVariable(ForgotpasswordController.ID) final long id,
                                         @PathVariable(ForgotpasswordController.TOKEN) final String token,
                                         @Validated({User.ValidReset.class}) @ModelAttribute(USER) User userPassword,
                                         BindingResult result,
                                         RedirectAttributes redirectAttributes) {

        logger.info("User token to change password : " + id + " : token " + token);
        PasswordResetToken passwordResetToken = userService.getPasswordResetToken(token);
        model.addAttribute("token", token);

        if (result.hasErrors()) {
            logger.info("User token to change password, errors 1 : ");
            return addAttributesAndReturnForm(model, userPassword, passwordResetToken);
        }

        logger.info("User token to change password, : not match " + userPassword.getPassword() +" : "+
                userPassword.getConfirmPassword());

        if(!(userPassword.getPassword().equals(userPassword.getConfirmPassword()))){
            logger.info("User token to change password, errors 2 : not match ");
            MessageHelper.addErrorAttribute(redirectAttributes, "non.unique.password");
            logger.info(FORGOT_PASSWORD + "/" + id + "/" + token );
            return "redirect:" + FORGOT_PASSWORD + "/" + id + "/" + token ;
        }

        logger.info("User token to change password, see email : " + id);







        try {

            logger.info("User token to change password, see email : entered try catch " + id);

            User user =passwordResetToken.getUser();
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, userDetailsService.loadUserByUsername(user.getEmail()).getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);

            logger.info("User token to change password, see email : After set auth");
            User userAuth = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            userService.changeUserPassword(userAuth, userPassword.getPassword());

            logger.info("User token to change password, saved : ");
        }catch (NullPointerException e){


            MessageHelper.addErrorAttribute(redirectAttributes, "auth.message.invalidToken");

            return "redirect:" + FORGOT_PASSWORD + "/" + id + "/" + token ;

        }catch (Exception e){
            logger.info("User token to change password, errors 3 : exception");
            MessageHelper.addErrorAttribute(redirectAttributes, "message.error");
            return "redirect:" + FORGOT_PASSWORD  + "/" + id + "/" + token ;
        }
        logger.info("User token to change password, sucess 4 : ");
        MessageHelper.addSuccessAttribute(redirectAttributes, "users.save.update");

        return "redirect:" + FORGOT_PASSWORD + "/" + id + "/" + token ;
    }

    private final SimpleMailMessage constructResetTokenEmail(final String contextPath, final Locale locale,
                                                             final String token, final User user) {
        final String url = contextPath + "/forgotpassword/" + user.getId() + "/" + token + "";
        logger.info("SENDING MAIL: Construct : " + user.getEmail());
        final String message = messages.getMessage("message.resetPassword", null, locale);
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Reset Password");
        email.setText(message + " \r\n" + url);
        logger.info("SENDING MAIL: Construct 2 : " + user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        logger.info("SENDING MAIL: Construct 3 : " + user.getEmail());
        return email;
    }
}
