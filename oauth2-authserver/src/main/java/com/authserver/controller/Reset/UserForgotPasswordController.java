package com.authserver.controller.Reset;

import com.authserver.model.reset.PasswordResetToken;
import com.authserver.model.user.User;
import com.authserver.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ui.Model;

/**
 * Created by Fholisani.Mashegana on 8/14/2016.
 */
public abstract class UserForgotPasswordController {
    public static final String USER = "user";
    public static final String USER_RESET = "userReset";
    public static final String ID = "id";
    public static final String TOKEN = "token";

    @Autowired
    protected JavaMailSenderImpl mailSender;

    @Autowired
    protected MessageSource messages;
    @Autowired
    protected Environment env;
    @Autowired
    protected UserDetailsService userDetailsService;

    @Autowired
    protected UserService userService;

    public void addAttributes(Model model, User user, PasswordResetToken passwordResetToken){
        model.addAttribute(USER, user);
        model.addAttribute(USER_RESET, passwordResetToken);
    }
}
