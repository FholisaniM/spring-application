package com.authserver.controller.Reset;

import com.authserver.model.reset.PasswordResetToken;
import com.authserver.model.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * Created by Fholisani.Mashegana on 8/16/2016.
 */
@Controller
//@RequestMapping(value = ForgotpasswordController.CHANGE_PASSWORD)
public class changePasswordController extends  UserForgotPasswordController {

    //@RequestMapping(method = RequestMethod.GET)
    public String  changePassword(Model model) {

        User user = new User();
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        return addAttributesAndReturnChangeForm(model, user, passwordResetToken);
    }

    private String addAttributesAndReturnChangeForm(Model model, User user, PasswordResetToken passwordResetToken){
        addAttributes(model, user, passwordResetToken);
        return  ForgotpasswordController.CHANGE_PASSWORD;
    }
}
