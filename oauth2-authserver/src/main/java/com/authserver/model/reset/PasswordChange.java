package com.authserver.model.reset;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
public class PasswordChange {

    private String newpassword;


    private String passConfirm;

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getPassConfirm() {
        return passConfirm;
    }

    public void setPassConfirm(String passConfirm) {
        this.passConfirm = passConfirm;
    }
}
