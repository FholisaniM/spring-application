package com.authserver.model.reset;

import javax.validation.constraints.NotNull;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
public class ResetPassword {
    @NotNull
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

