package com.authserver.model.user;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Data
@Entity
@Table(name = "user_address")
public class Address {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private String street;
    private String suburb;
    @Column(name="CITY", nullable=false)
    private String city;
    private String postalCode;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }



    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Address() {
    }


    public Address(Address address) {
        this.id = address.id;
        this.street = address.street;
        this.suburb = address.suburb;
        this.city = address.city;
        this.postalCode = address.postalCode;

    }





}

