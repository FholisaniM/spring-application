package com.authserver.repository;

import com.authserver.model.user.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    Address findByCity(String city);
}
