package com.authserver.service.implementation;

import com.authserver.Oauth2AuthserverApplication;
import com.authserver.model.user.User;
import com.authserver.model.user.UserProfile;
import com.authserver.service.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Autowired
    public CustomUserDetailsService(UserService userService) {
        this.userService = userService;
    }
    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);

    @Override
    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.findByEmail(email);
        logger.info("Email :::::: " + email);
        System.out.println("User : "+user);
        if(user==null){
            logger.info("User cannot be found :::::: " + email);
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        logger.info("User found :::::: " + user.getEmail() + "  : " + user.getPassword());
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                user.getState().equals("Active"), true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        logger.info("About to return authorities : initial");
        for(UserProfile userProfile : user.getUserProfiles()){
            System.out.println("UserProfile : "+userProfile);
            logger.info("About to return authorities : adding role");
            authorities.add(new SimpleGrantedAuthority("ROLE_"+userProfile.getType()));
        }
        System.out.print("authorities :" + authorities);
        logger.info("About to return authorities : passed : "+ authorities.toString());
        return authorities;
    }



}
