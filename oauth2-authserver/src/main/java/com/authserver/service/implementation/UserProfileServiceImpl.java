package com.authserver.service.implementation;

import com.authserver.model.user.UserProfile;
import com.authserver.repository.UserProfileRepository;
import com.authserver.service.interfaces.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {
    @Autowired
    UserProfileRepository userProfileRepository;

    @Override
    public List<UserProfile> findAll() {
        return  (List<UserProfile>) userProfileRepository.findAll();
    }

    @Override
    public UserProfile findByType(String type) {
        for(UserProfile userProfile : findAll()){
            if(userProfile.getType().equalsIgnoreCase(type)){
                return userProfile;
            }
        }
        return null;
    }

    @Override
    public UserProfile findById(long id) {
        return userProfileRepository.findOne(id);
    }
}