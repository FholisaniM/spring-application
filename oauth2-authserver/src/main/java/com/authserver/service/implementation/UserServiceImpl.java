package com.authserver.service.implementation;

import com.authserver.Oauth2AuthserverApplication;
import com.authserver.model.reset.PasswordResetToken;
import com.authserver.model.user.User;
import com.authserver.repository.AddressRepository;
import com.authserver.repository.PasswordResetTokenRepository;
import com.authserver.repository.UserRepository;
import com.authserver.service.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    PasswordResetTokenRepository passwordTokenRepository;

    static private Logger logger = LoggerFactory.getLogger(Oauth2AuthserverApplication.class);


    @Override
    public void save(User user) {
        user.setJoiningDate(user.calculateJoiningDate(0));
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.setConfirmPassword(new BCryptPasswordEncoder().encode(user.getConfirmPassword()));
        addressRepository.save(user.getAddress());
        userRepository.save(user);


    }

    @Override
    public void delete(User user) {
        userRepository.delete(user.getId());
        addressRepository.delete(user.getAddress().getId());


    }

    @Override
    public void updateUser(User currentUser, User user) {
        currentUser.setFirstName(user.getFirstName());
        currentUser.setState(user.getState());
        currentUser.setUserProfiles(user.getUserProfiles());
        currentUser.setEmail(user.getEmail());
        currentUser.setAddress(user.getAddress());
        addressRepository.save(currentUser.getAddress());
        userRepository.save(currentUser);
    }


    @Override
    public User findById(long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByEmail(String email) {

        /*for(User user : findAllUsers()){
            if(user.getEmail().equalsIgnoreCase(email)){
                return user;
            }
        }*/
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public Page<User> findAllUserMembers(Pageable pageable) {
        Page<User> pageResult = userRepository.findAll(pageable);
        return pageResult;
    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
       final PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.save(myToken);

    }

    @Override
    public PasswordResetToken getPasswordResetToken(String token) {
        return passwordTokenRepository.findByToken(token);
    }

    @Override
    public void changeUserPassword(User user, String password) {

        user.setPassword(new BCryptPasswordEncoder().encode(password));
        userRepository.save(user);
    }

    @Override
    public boolean isUserEmailUnique(long id, String email) {
        User user = findByEmail(email);
        return ((user == null) || ( (user.getId() == id)));
    }

    @Override
    public boolean isUserPasswordMatch(String password, String confirmPassword) {
        return (!(password.equals(confirmPassword)));
    }


}