package com.authserver.service.interfaces;

import com.authserver.model.user.UserProfile;

import java.util.List;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
public interface UserProfileService {
    List<UserProfile> findAll();

    UserProfile findByType(String type);

    UserProfile findById(long id);
}

