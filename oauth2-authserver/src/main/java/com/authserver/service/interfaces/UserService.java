package com.authserver.service.interfaces;

import com.authserver.model.reset.PasswordResetToken;
import com.authserver.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Fholisani.Mashegana on 8/13/2016.
 */
public interface UserService {
    void save(User user);
    void delete(User user);
    void updateUser(User currentUser, User user);

    User findById(long id);

    User findByEmail(String email);
    List<User> findAllUsers();
    Page<User> findAllUserMembers(Pageable pageable);
    void createPasswordResetTokenForUser(User user, String token);
    PasswordResetToken getPasswordResetToken(String token);
    void changeUserPassword(User user, String password);
    boolean isUserEmailUnique(long id, String email);
    boolean isUserPasswordMatch(String password, String confirmPassword);

}
