/**
 * Created by Fholisani.Mashegana on 8/18/2016.
 */
$("#myForm").submit(function( event ) {
    $("#mySubmitButton").prop("disabled", true).addClass("disabled");
});

$("#myFormLogin").submit(function( event ) {
    $("#myLoginButton").prop("disabled", true).addClass("disabled");
});

$("#formChangePassword").submit(function( event ) {
    $("#myChangePassword").prop("disabled", true).addClass("disabled");
});