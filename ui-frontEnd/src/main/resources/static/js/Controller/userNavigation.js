/**
 * Created by Fholisani.Mashegana on 6/5/2016.
 */



'use strict';

App.controller('navigation',

    function($rootScope, $http, $location, $route) {

        var self = this;

        self.tab = function(route) {
            return $route.current && route === $route.current.controller;
        };

        $http.get('user').then(function(response) {
            if (response.data.name) {
                $rootScope.authenticated = true;
                $rootScope.signedInUser = response.data.name;
            } else {
                $rootScope.authenticated = false;
            }
        }, function() {
            $rootScope.authenticated = false;
        });

        self.credentials = {};

        self.logout = function() {
            $http.post('logout', {}).finally(function() {
                $rootScope.authenticated = false;
                $location.path("/");
            });
        }

    }).controller('home', function($http) {
    var self = this;
    $http.get('resource/').then(function(response) {
        self.greeting = response.data;
    })
});
