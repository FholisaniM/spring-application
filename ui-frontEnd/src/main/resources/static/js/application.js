/**
 * Created by Fholisani.Mashegana on 5/21/2016.

 */




'use strict';

var App = angular.module('application', ['ngRoute', 'ui.bootstrap']).config(function ($routeProvider, $httpProvider) {
//Add Navigation to the Angular Application
    $routeProvider.when('/', {
        templateUrl: 'home.html',
        controller: 'home',
        controllerAs: 'controller'
    }).when('/users', {
        templateUrl: 'templates/users.html',
        controller: 'users',
        controllerAs: 'controller'
    }).otherwise('/');

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    $httpProvider.defaults.headers.common['Accept'] = 'application/json';


}).filter('pages', function () {
    return function (input, currentPage, totalPages, range) {
        currentPage = parseInt(currentPage);
        totalPages = parseInt(totalPages);
        range = parseInt(range);

        var minPage = (currentPage - range < 0) ? 0 : (currentPage - range > (totalPages - (range * 2))) ? totalPages - (range * 2) : currentPage - range;
        var maxPage = (currentPage + range > totalPages) ? totalPages : (currentPage + range < range * 2) ? range * 2 : currentPage + range;

        for (var i = minPage; i < maxPage; i++) {
            input.push(i);
        }

        return input;
    };
});