/**
 * Created by Fholisani.Mashegana on 7/3/2016.
 */
'use strict';
App.controller('users', ['$scope', '$http', 'UserService', '$location', function ($scope, $http, UserService, $location) {
    var self = this;
    self.user = {
        id: null, firstName: '', email: '', password: '',
        confirmPassword: '', "address": {
            "id": null,
            "street": '',
            "suburb": '',
            "city": '',
            "postalCode": null
        },
        state: '',
        joiningDate: new Date(),
        userProfiles: []
    };
    var userRoles = [
        {"id": 1, "type": "USER"},
        {"id": 2, "type": "ADMIN"},
        {"id": 3, "type": "DBA"}];

    var state = [
        {"id": 1, "type": "Active"},
        {"id": 2, "type": "Inactive"}];

    self.UsersGrid = [];

    self.users = [];


    self.fetchAllUsers = function (def, filter) {
        UserService.fetchAllUsers()
            .then(
                function (data) {
                    self.users = data;
                    self.UserAdapter(self.users);
                    data = $.grep(self.UsersGrid, function (user) {
                        return (!filter.firstName || user.firstName.indexOf(filter.firstName) > -1)
                            && (!filter.email || user.email === filter.email)

                    });

                    def.resolve(data);



                },
                function (errResponse) {


                    console.error('Error while fetching Currencies');
                }
            );

    };


    self.UserStateAdapter = function (state) {

        if (state === "Active") {
            return 1;
        } else {
            return 2;
        }
    },

        self.UserRoleAdapter = function (userProfiles) {

            var largest = 0;

            userProfiles.map(function (obj) {
                if (obj.id > largest) largest = obj.id;
            });
            return largest;
        },
        self.createUserStateAdapter = function (state) {

            if (state === 1) {
                return "Active";
            } else {
                return "Inactive";
            }
        },
        self.createUserRoleAdapter = function (userProfile) {
            var profile = $.grep(userRoles, function (role) {
                return (role.id === userProfile);

            });

            return profile;
        },


        self.UserAdapter = function (users) {
            var gridInfo =[];

            $.each(users, function (i, user) {
                var userGrid = {
                    id: user.id,
                    firstName: user.firstName,
                    email: user.email,
                    password: user.password,
                    confirmPassword:user.confirmPassword,
                    "address": {
                        "id":  user.address.id,
                        "street": user.address.street,
                        "suburb":user.address.suburb,
                        "city": user.address.city,
                        "postalCode": user.address.postalCode,
                    },
                    state: self.UserStateAdapter(user.state),
                    joiningDate: user.joiningDate,
                    userProfiles: self.UserRoleAdapter(user.userProfiles),
                }



                gridInfo.push(userGrid);


            });

          self.UsersGrid = gridInfo;
        };


    self.reset = function () {
        self.user = {
            id: null, firstName: '', email: '', password: '',
            confirmPassword: '', "address": {
                "id": null,
                "street": '',
                "suburb": '',
                "city": '',
                "postalCode": null
            },
            state: '',
            joiningDate: new Date(),
            userProfiles: []
        };
        $scope.myForm.$setPristine(); //reset Form
    };


    self.updateUser = function (def, user, id) {
        UserService.updateUser(user, id)
            .then(

                function(response){
                    def.resolve(response);
                    $("#jsGrid").jsGrid("loadData");
                    self.resetMsg();
                },
                function(errResponse){
                    self.resetMsg();
                    //console.error('Error while creating user');
                    return errResponse;


                }

            );
    };

    self.deleteUser = function (user, def) {
        UserService.deleteUser(user.id)
            .then(
                function(response){
                    def.resolve(response);

                     $("#jsGrid").jsGrid("loadData");
                },
                function(errResponse){

                   // console.error('Error while creating user');
                    return errResponse;


                }


            );

    };

    self.createUser = function (user, def) {
        UserService.createUser(user)
            .then(
                function(response){
                    def.resolve(response);
                    $("#jsGrid").jsGrid("loadData");
                    self.resetMsg();
                    self.reset();
                },
                function(errResponse){
                    $scope.isCreateSeccesful = false;
                    $scope.isErrorResponseMsg = true;

                    if(errResponse.data != "" || errResponse.data !=null){
                        $scope.errorResponseMsg = errResponse.data.errorMessage;
                        $scope.detailErrorResponse = errResponse.data.errors;
                    }else{
                        $scope.errorResponseMsg = "User could not be created, contact your system administrator";
                        $scope.detailErrorResponse = ["Create Error."];
                    }
                    self.resetMsg();

                    console.error('Error while creating user');
                    return errResponse;
                }
            );


    };


    self.resetMsg = function () {


        angular.element(document).ready(function () {
            $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
                // $("#success-alert").alert('close');

                $("#success-alert").on("close.bs.alert", function () {
                    $("#success-alert").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });


            });
            $("#danger-alertService").fadeTo(5000, 500).slideUp(500, function(){

                $("#danger-alertService").on("close.bs.alert", function () {
                    $("#danger-alertService").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });
            });
            $("#danger-alertUpdate").fadeTo(5000, 500).slideUp(500, function(){

                $("#danger-alertUpdate").on("close.bs.alert", function () {
                    $("#danger-alertUpdate").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });
            });

            $("#success-alertCreate").fadeTo(5000, 500).slideUp(500, function(){
                $("#success-alertCreate").on("close.bs.alert", function () {
                    $("#success-alertCreate").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });
            });
            $("#danger-alertError").fadeTo(5000, 500).slideUp(500, function(){
                $("#danger-alertError").on("close.bs.alert", function () {
                    $("#danger-alertError").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });
            });

            $("#warning-alert").fadeTo(5000, 500).slideUp(500, function(){
                $("#warning-alert").on("close.bs.alert", function () {
                    $("#warning-alert").hide(); //hide the alert
                    return false; //don't remove it from DOM
                });
            });
        });



    };

    self.resetLoadMsg = function () {


        $("#success-alert").hide();
        $("#danger-alertService").fadeTo(10000, 500).slideUp(500, function(){

            $("#danger-alertService").on("close.bs.alert", function () {
                $("#danger-alertService").hide(); //hide the alert
                return false; //don't remove it from DOM
            });
        });
        $("#danger-alertUpdate").hide();

        $("#success-alertCreate").hide();
        $("#danger-alertError").hide();

        $("#warning-alert").hide();


    };

    self.createUserAdapter = function (gridUser) {



        self.user.id = gridUser.id;
        self.user.firstName = gridUser.firstName;
        self.user.email = gridUser.email;
        self.user.password = gridUser.firstName;//gridUser.password;
        self.user.confirmPassword =gridUser.firstName;// gridUser.confirmPassword;
        self.user.address.id = gridUser.address.id;
        self.user.address.street = gridUser.address.street;
        self.user.address.suburb = gridUser.address.suburb;
        self.user.address.city = gridUser.address.city;
        self.user.address.postalCode = gridUser.address.postalCode;
        self.user.state =self.createUserStateAdapter(gridUser.state);
        self.user.joiningDate =gridUser.joiningDate;

        var gridUserRole = self.createUserRoleAdapter(gridUser.userProfiles);
        self.user.userProfiles.push(gridUserRole[0]);

        return self.user;

    };


    var DateField = function (config) {
        jsGrid.Field.call(this, config);
    };

    DateField.prototype = new jsGrid.Field({
        sorter: function (date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function (value) {
            return new Date(value).toDateString();
        },

        filterTemplate: function () {
            var now = new Date();
            this._fromPicker = $("<input>").datepicker({defaultDate: now.setFullYear(now.getFullYear() - 1)});
            this._toPicker = $("<input>").datepicker({defaultDate: now.setFullYear(now.getFullYear() + 1)});
            return $("<div>").append(this._fromPicker).append(this._toPicker);
        },

        insertTemplate: function (value) {
            return this._insertPicker = $("<input>").datepicker({defaultDate: new Date()});
        },

        editTemplate: function (value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },

        insertValue: function () {
            if (this._insertPicker.datepicker("getDate") !== null) {
                return this._insertPicker.datepicker("getDate").toISOString();
            } else {
                return this._insertPicker.datepicker("getDate")
            }


        },

        editValue: function () {
            if (this._editPicker.datepicker("getDate") !== null) {
                return this._editPicker.datepicker("getDate").toISOString();
            } else {
                return this._editPicker.datepicker("getDate")

            }

        },

        filterValue: function () {
            return {
                from: this._fromPicker.datepicker("getDate"),
                to: this._toPicker.datepicker("getDate")
            };
        }
    });

    jsGrid.fields.date = DateField;





    $(function () {

        var grid =  $("#jsGrid").jsGrid({
            height: "auto",
            width: "100%",

            sorting: true,
            paging: true,
            autoload: true,
            filtering: true,
            inserting: true,
            editing: true,
            pageSize: 10,
            pageButtonCount: 5,

            controller: {
                loadData: function (filter) {

                    var def = $.Deferred();
                    self.fetchAllUsers(def, filter);

                    return def.promise();
                },

                insertItem: function (item) {

                    var def = $.Deferred();
                    var newuser = self.createUserAdapter(item);

                    self.createUser(newuser, def);

                    setTimeout(function () {
                        console.log('insertItem');
                        grid.insertFailed = true;
                        def.resolve();


                    },2);

                    return  def.promise();

                    //this.users.push(item);
                },
                updateItem: function (item) {



                    var def = $.Deferred();
                    var updateUser = self.createUserAdapter(item);
                    self.updateUser(def, updateUser, updateUser.id );
                    return  def.promise();

                    //alert(" About to update");
                    //this.users.push(item);
                },

                deleteItem: function (item) {
                    var def = $.Deferred();
                    self.deleteUser(item, def);
                    return  def.promise();
                }
            },
            fields: [
                {name: "id", title: "ID", width: 30,},

                {name: "firstName", title: "Name", type: "text", width: 100, validate: "required"},
                {
                    name: "email", title: "Email", type: "text", width: 150,
                    validate: {
                        validator: function (value, item) {
                            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                            return re.test(value);
                        },
                        message: "Invalid Email"
                    }


                },
                {
                    name: "state",
                    title: "Status",
                    type: "select",
                    items: state,
                    valueField: "id",
                    textField: "type",
                    width: 100
                },
                {
                    name: "userProfiles",
                    title: "Role",
                    type: "select",
                    items: userRoles,
                    valueField: "id",
                    textField: "type",
                    width: 100
                },
                {
                    name: "joiningDate", title: "Start Date", type: "date", width: 100, align: "center",
                    validate: {
                        validator: function (value, item) {
                            if (value !== null) {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        message: "Invalid Date"
                    }
                },
                {name: "address.street", title: "Street", type: "text", width: 100, validate: "required"},
                {name: "address.suburb", title: "Suburb", type: "text", width: 100, validate: "required"},
                {name: "address.city", title: "City", type: "text", width: 100, validate: "required"},
                {name: "address.postalCode", title: "Code", type: "number", width: 100, validate: "required"},
                {name: "password", type: "text", visible: false, width: 0},
                {name: "confirmPassword", type: "text", visible: false, width: 0},


                {type: "control"}
            ]


        });

    });


}]);
