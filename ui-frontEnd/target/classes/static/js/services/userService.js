/**
 * Created by Fholisani.Mashegana on 6/5/2016.
 */
'use strict';

App.factory('UserService', ['$http', '$q', '$rootScope', '$location', '$route',
    function ($http, $q, $rootScope, $location, $route) {


        return {

            fetchAllUsers: function () {
                return $http.get('users')
                    .then(
                        function (response) {

                            //  $rootScope.isServiceErrorResponseMsg = false;
                            $rootScope.isErrorResponseMsg = false;
                            $rootScope.isCreateSeccesful = false;
                            $rootScope.dataLoading = false;
                            $rootScope.userUpdatedSeccesful = false;
                            $rootScope.resetPasswordMailDeliveredNotFound = false;
                            $rootScope.resetPasswordMailDelivered = false;
                            $rootScope.resetPasswordMailSeccesful = false;


                            $rootScope.isServiceErrorResponseMsg = false;
                            return response.data;
                        },
                        function (errResponse) {
                            $rootScope.isServiceErrorResponseMsg = true;
                            $rootScope.serviceErrorResponseMsg = "Pease check Internet connection or contact your system Administrator";

                            if(errResponse.status = 401){
                                $rootScope.serviceErrorResponseMsg = "Your session expired, please login to proceed";

                            }
                            console.error('Error while fetching users');
                            return errResponse;
                        }
                    );
            },

            createUser: function (user) {
                return $http.post('user/', user)
                    .then(
                        function (response) {
                            $rootScope.isErrorResponseMsg = false;
                            $rootScope.isCreateSeccesful = true;
                            $rootScope.createSeccesfulMsg = "User created Succesfully  ";
                            return response.data;
                        },
                        function (errResponse) {
                            $rootScope.isCreateSeccesful = false;
                            $rootScope.isErrorResponseMsg = true;

                            if (errResponse.data != "" || errResponse.data != null) {
                                $rootScope.errorResponseMsg = errResponse.data.errorMessage;
                                $rootScope.detailErrorResponse = errResponse.data.errors;
                            } else {
                                $rootScope.errorResponseMsg = "User could not be created, contact your system administrator";
                                $rootScope.detailErrorResponse = ["Create Error."];
                            }

                            console.error('Error while creating user');


                            return errResponse;
                        }
                    );
            },


            resetPasswordUser: function (resetPasswordModel) {
                return $http.post('http://localhost:8080/basic/resetPassword/', resetPasswordModel)
                    .then(
                        function (response) {
                            $rootScope.dataLoading = false;
                            $rootScope.resetPasswordMailDeliveredNotFound = false;
                            $rootScope.resetPasswordMailDelivered = false;
                            $rootScope.resetPasswordMailSeccesful = true;
                            $rootScope.resetPasswordMailSeccesfulMsg = "Email Succesfully Sent ";
                            return response.data;

                        },
                        function (errResponse) {
                            $rootScope.dataLoading = false;
                            if (errResponse.status === 404) {
                                $rootScope.resetPasswordMailDeliveredNotFound = true;
                                $rootScope.resetPasswordMailDeliveredNotFoundError = "User with the provided email not found";
                            } else {
                                $rootScope.resetPasswordMailSeccesful = false;
                                $rootScope.resetPasswordMailDelivered = true;
                                $rootScope.resetPasswordMailDeliveredError = "Error email could not be delivered";


                            }


                            console.error('Error while reseting password user');
                            return errResponse;
                        }
                    );
            },


            updatePassword: function (userPasswordModel) {
                return $http.post('http://localhost:8080/basic/user/savePassword/', userPasswordModel)
                    .then(
                        function (response) {
                            $rootScope.passwordUpdated = false;

                            $rootScope.passwordUpdatedSeccesful = true;
                            $rootScope.passwordUpdatedSeccesfulMsg = "Password Updated Successfully";

                            return response.data;
                        },
                        function (errResponse) {
                            $rootScope.passwordUpdatedSeccesful = false;
                            $rootScope.passwordUpdated = true;
                            $rootScope.passwordUpdatedError = "Error while updating password user";
                           // console.error('Error while updating password user');
                            return errResponse;
                        }
                    );
            },


            updateUser: function (user, id) {
                return $http.put('user/' + id + '/update', user)
                    .then(
                        function (response) {
                            $rootScope.userUpdated = false;

                            $rootScope.userUpdatedSeccesful = true;
                            $rootScope.userUpdatedSeccesfulMsg = "User Updated Successfully";

                            return response.data;
                        },
                        function (errResponse) {
                            $rootScope.userUpdatedSeccesful = false;

                            $rootScope.userUpdated = true;


                            $rootScope.userUpdatedError = "Error while updating  user";
                           // console.error('Error while updating user');
                            return errResponse;
                        }
                    );
            },

            deleteUser: function (id) {
                return $http.delete('user/' + id + '/remove')
                    .then(
                        function (response) {
                            return response.data;
                        },
                        function (errResponse) {
                            console.error('Error while deleting user');
                            return errResponse;
                        }
                    );
            }

        };

    }]);
